package com.example.rafika.belajar;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Button btn9;
    private Button btn8;
    private Button btn7;
    private Button btn6;
    private Button btn5;
    private Button btn4;
    private Button btn3;
    private Button btn2;
    private Button btn1;
    private Button btn0;
    private Button btn_tambah;
    private Button btn_kurang;
    private Button btn_kali;
    private Button btn_bagi;
    private Button btnTitik;
    private Button btn_sama_dengan;
    private Button btn_clear;
    private TextView tvControl;
    private TextView tvResult;
    private final char TAMBAH = '+';
    private final char KURANG = '-';
    private final char BAGI = '/';
    private final char KALI = '*';
    private double val1 = Double.NaN;
    private double val2;
    private char ACTION;
    private final char EQU = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUIView();
        setBtnAngka();
        setBtnOperatorAritmatika();

    }

    public void setUIView() {
        btn9 = findViewById(R.id.btn9);
        btn8 = findViewById(R.id.btn8);
        btn7 = findViewById(R.id.btn7);
        btn6 = findViewById(R.id.btn6);
        btn5 = findViewById(R.id.btn5);
        btn4 = findViewById(R.id.btn4);
        btn3 = findViewById(R.id.btn3);
        btn2 = findViewById(R.id.btn2);
        btn1 = findViewById(R.id.btn1);
        btn0 = findViewById(R.id.btn0);
        btn_tambah = findViewById(R.id.btnTambah);
        btn_kurang = findViewById(R.id.btnKurang);
        btn_kali = findViewById(R.id.btnKali);
        btn_bagi = findViewById(R.id.btnBagi);
        btnTitik = findViewById(R.id.btnTitik);
        btn_clear = findViewById(R.id.btnClear);
        tvControl = findViewById(R.id.tvControl);
        tvResult = findViewById(R.id.tvResult);
        btn_sama_dengan = findViewById(R.id.btnSamaDengan);
    }


    public void setBtnAngka(){
        btn0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvControl.setText(tvControl.getText().toString() + "0");
            }
        });

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvControl.setText(tvControl.getText().toString() + "1");
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvControl.setText(tvControl.getText().toString() + "2");
            }
        });

        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvControl.setText(tvControl.getText().toString() + "3");
            }
        });

        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvControl.setText(tvControl.getText().toString() + "4");
            }
        });

        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvControl.setText(tvControl.getText().toString() + "5");
            }
        });

        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvControl.setText(tvControl.getText().toString() + "6");
            }
        });

        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvControl.setText(tvControl.getText().toString() + "7");
            }
        });

        btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvControl.setText(tvControl.getText().toString() + "8");
            }
        });

        btn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvControl.setText(tvControl.getText() + "9");
            }
        });
    }


    public void setBtnOperatorAritmatika(){
        btn_tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compute();
                ACTION = TAMBAH;
                tvResult.setText(String.valueOf(val1) + " + ");
                tvControl.setText(null);
            }
        });

        btn_kurang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compute();
                ACTION = KURANG;
                tvResult.setText(String.valueOf(val1) + " - ");
                tvControl.setText(null);
            }
        });

        btn_kali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compute();
                ACTION = KALI;
                tvResult.setText(String.valueOf(val1) + " * ");
                tvControl.setText(null);
            }
        });

        btn_bagi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compute();
                ACTION = BAGI;
                tvResult.setText(String.valueOf(val1) +  "/ ");
                tvControl.setText(null);
            }
        });

        btn_sama_dengan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compute();
                ACTION = EQU;
                tvResult.setText(tvResult.getText().toString() + String.valueOf(val2) + "=" + String.valueOf(val1));
                tvControl.setText(String.valueOf(val1));
            }
        });

        btn_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tvControl.getText().length()>0){
                    CharSequence name = tvControl.getText().toString();
                    tvControl.setText(name.subSequence(0, name.length()-1));
                } else{
                    val1 = Double.NaN;
                    val2 = Double.NaN;
                    tvControl.setText(null);
                    tvResult.setText(null);
                }
            }
        });
    }

    private void compute() {
        if (!Double.isNaN(val1)) {
            val2 = Double.parseDouble(tvControl.getText().toString());
            switch (ACTION) {
                case TAMBAH:
                    val1 += val2;
                    break;
                case KURANG:
                    val1 -= val2;
                    break;
                case KALI:
                    val1 *= val2;
                    break;
                case BAGI:
                    val1 /= val2;
                    break;
                case EQU:
                    break;
            }
        } else {
            val1 = Double.parseDouble(tvControl.getText().toString());
        }

    }


}
